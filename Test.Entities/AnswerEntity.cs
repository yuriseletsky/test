﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Entities
{
    public class AnswerEntity
    {
        public int Id { get; set; }
        public string Answer { get; set; }
        public float Scores { get; set; }
        public int TestItemId { get; set; }
    }
}

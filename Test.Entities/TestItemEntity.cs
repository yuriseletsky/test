﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Entities
{
    public class TestItemEntity
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public int TestId { get; set; }
    }
}

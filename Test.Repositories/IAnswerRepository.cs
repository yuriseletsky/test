﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositories
{
    public interface IAnswerRepository
    {
        List<AnswerEntity> GetAll();
        void Add(AnswerEntity newItem);
        void Delete(int id);
        void Edit(AnswerEntity editItem);
    }
}

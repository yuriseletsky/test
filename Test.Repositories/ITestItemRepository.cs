﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositories
{
    public interface ITestItemRepository
    {
        List<TestItemEntity> GetAll();
        List<TestItemEntity> GetItemsWhereTestId(int testId);
        void Add(TestItemEntity newItem);
        void Delete(int id);
        void Edit(TestItemEntity editItem);
        TestItemEntity GetItemById(int id);
        void DeleteItemByTestId(int id);
    }
}

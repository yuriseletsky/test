﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositories
{
    public interface ITestRepository
    {
        List<TestEntity> GetAll();
        TestEntity GetElementById(int id);
        void Add(TestEntity newItem);
        void Delete(int id);
        void EditTask(TestEntity editItem);
    }
}

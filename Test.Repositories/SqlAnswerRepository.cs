using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Test.Entities;

namespace Test.Repositories
{
    public class SqlAnswerRepository : IAnswerRepository
    {
        private static List<AnswerEntity> _sqlData = new List<AnswerEntity>();
        private readonly string _connectionString;

        public SqlAnswerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<AnswerEntity> GetAll()
        {
            _sqlData = new List<AnswerEntity>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT [Id], [Answer], [Scores] FROM [dbo].[Answers]";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var sqlTask = new AnswerEntity
                            {
                                Id = (int)reader["Id"],
                                Answer = (string)reader["Answer"],
                                Scores = (float)reader["Scores"]
                            };
                            _sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            return _sqlData;
        }

        public void Add(AnswerEntity newItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO [dbo].[Answers] ([Answer],[Scores]) VALUES (@Answer, @Scores);";
                    command.Parameters.AddWithValue("@Answer", newItem.Answer);
                    command.Parameters.AddWithValue("@Scores", newItem.Scores);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "DELETE FROM [dbo].[Answers] WHERE [Id] = @Id";
                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void Edit(AnswerEntity editItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE [dbo].[Answers] SET [Answers] = @Answers, [Scores] = @Scores";
                    command.Parameters.AddWithValue("@Answers", editItem.Answer);
                    command.Parameters.AddWithValue("@Scores", editItem.Scores);
                    command.ExecuteNonQuery();
                }
            } 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Test.Entities;

namespace Test.Repositories
{
    public class SqlTestItemRepository : ITestItemRepository
    {
        private static List<TestItemEntity> _sqlData = new List<TestItemEntity>();
        private readonly string _connectionString;

        public SqlTestItemRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<TestItemEntity> GetAll()
        {
            _sqlData = new List<TestItemEntity>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT [Id], [Question], [TestId] FROM [dbo].[TestItems]";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var sqlTask = new TestItemEntity
                            {
                                Id = (int)reader["Id"],
                                Question = (string)reader["Question"],
                                TestId = (int)reader["TestId"]
                            };
                            _sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            return _sqlData;
        }

        public List<TestItemEntity> GetItemsWhereTestId(int testId)
        {
            return GetAll().Where(i=>i.TestId==testId).ToList();
        }

        public TestItemEntity GetItemById(int id)
        {
            return GetAll().FirstOrDefault(i => i.Id == id);
        }

        public void Add(TestItemEntity newItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO [dbo].[TestItems] ([Question],[TestId]) VALUES (@Question, @TestId);";
                    command.Parameters.AddWithValue("@Question", newItem.Question);
                    command.Parameters.AddWithValue("@TestId", newItem.TestId);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "DELETE FROM [dbo].[TestItems] WHERE [Id] = @Id";
                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void DeleteItemByTestId(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "DELETE FROM [dbo].[TestItems] WHERE [TestId] = @Id";
                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Edit(TestItemEntity editItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE [dbo].[TestItems] SET [Question] = @Question, [TestId] = @TestId";
                    command.Parameters.AddWithValue("@Question", editItem.Question);
                    command.Parameters.AddWithValue("@TestId", editItem.TestId);
                    command.ExecuteNonQuery();
                }
            } 
        }
    }
}
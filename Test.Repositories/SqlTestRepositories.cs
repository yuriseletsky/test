﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositories
{
    public class SqlTestRepositories : ITestRepository
    {
        private static List<TestEntity> _sqlData = new List<TestEntity>();
        private readonly string _connectionString;

        public SqlTestRepositories(string connectionString)
        {
            _connectionString = connectionString;
        }
                           
        public List<TestEntity> GetAll()
        {
            _sqlData = new List<TestEntity>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT [Id], [Name], [Description] FROM [dbo].[Tests]";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TestEntity sqlTask = new TestEntity
                            {
                                Id = (int)reader["Id"],
                                Name = (string)reader["Name"],
                                Description = (string)reader["Description"],
                            };
                            _sqlData.Add(sqlTask);
                        }
                    }
                }
            }
            return _sqlData;
        }

        public TestEntity GetElementById(int id)
        {
            return GetAll().FirstOrDefault(el => el.Id == id);
        }

        public void Add(TestEntity newItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "INSERT INTO [dbo].[Tests] ([Name],[Description]) VALUES (@Name, @Description);";
                    command.Parameters.AddWithValue("@Name", newItem.Name);
                    command.Parameters.AddWithValue("@Description", newItem.Description);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "DELETE FROM [dbo].Tests WHERE [Id] = @Id";
                    command.Parameters.AddWithValue("@Id", id);
                    command.ExecuteNonQuery();
                }
            } 
        }

        public void EditTask(TestEntity editItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "UPDATE [dbo].[Tests] SET [Name] = @Name, [Description] = @Description  WHERE [Id] = @Id";
                    command.Parameters.AddWithValue("@Name", editItem.Name);
                    command.Parameters.AddWithValue("@Description", editItem.Description);
                    command.Parameters.AddWithValue("@Id", editItem.Id);
                    command.ExecuteNonQuery();
                }
            } 
        }


    }
}

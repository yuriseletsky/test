using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using Test.Repositories;
using System.Configuration;

namespace Test.WebUI
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        var connectionString = ConfigurationManager.ConnectionStrings["TestDataBase"].ConnectionString;
        container.RegisterType<IAnswerRepository, SqlAnswerRepository>(new InjectionConstructor(connectionString));
        container.RegisterType<ITestItemRepository, SqlTestItemRepository>(new InjectionConstructor(connectionString));
        container.RegisterType<ITestRepository, SqlTestRepositories>(new InjectionConstructor(connectionString));

    }
  }
}
CREATE DATABASE TestDataBase

GO

USE TestDataBase

GO

CREATE TABLE Tests(
	Id int not null IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(128) not null,
	[Description] nvarchar(2048) not null,
);

CREATE TABLE TestItems(
	Id int not null IDENTITY(1,1) PRIMARY KEY,
	Question nvarchar(2048) not null,
	TestId int,
	FOREIGN KEY (TestId) REFERENCES Tests(Id)
);

CREATE TABLE Answers(
	Id int not null IDENTITY(1,1) PRIMARY KEY,
	Answer nvarchar(2048) not null,
	Scores float,
	TestItemId int, 
	FOREIGN KEY (TestItemId) REFERENCES TestItems(Id)
);


select * from Tests

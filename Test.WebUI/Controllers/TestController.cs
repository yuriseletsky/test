﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Test.Entities;
using Test.Repositories;

namespace Test.WebUI.Controllers
{
    public class TestController : Controller
    {
        private readonly ITestRepository _testRepository;
        private readonly ITestItemRepository _testItemRepository;

        public TestController(ITestRepository testRepository, ITestItemRepository testItemRepository)
        {
            _testRepository = testRepository;
            _testItemRepository = testItemRepository;
        }

        //
        // GET: /Test/

        public ActionResult Index()
        {
            var tasks = _testRepository.GetAll();
            ViewBag.Result = tasks;
            ViewBag.Error = null;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string nameTest, string description)
        {
            ViewBag.Error = "must be not empty or less 128 chars";
            if (nameTest.Length <= 128 && !string.IsNullOrWhiteSpace(nameTest))
            {
                var test = new Test.Entities.TestEntity()
                {
                    Name = nameTest,
                    Description = description
                };
                ViewBag.Error = null;
                _testRepository.Add(test);
            }
            ViewBag.Result = _testRepository.GetAll();
            return View();
        }


        [HttpGet]
        public ActionResult Show(int id)
        {
            ViewBag.Result = _testRepository.GetElementById(id); 
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var thisItem = _testRepository.GetElementById(id);
            Models.Test model = new Models.Test
            {
                Id = id,
                Name = thisItem.Name,
                Description = thisItem.Description,
                TestItems = _testItemRepository.GetItemsWhereTestId(id)
            };
            ViewBag.Result = model;
            return View("../TestItem/Edit");
        }

        [HttpPost]
        public ActionResult EditValue(string name, string description,int id, int? testItemsId)
        {
            _testRepository.EditTask(new TestEntity{Id = id,Name = name,Description = description});
            Models.Test model = new Models.Test
            {
                Id = id,
                Name = name,
                Description = description,
                TestItems = _testItemRepository.GetItemsWhereTestId(id)
            };
            ViewBag.Result = model;
            return Redirect(String.Format("Edit?id={0}", id));
        }

        [HttpPost]
        public ActionResult Delete(int testId)
        {
            _testItemRepository.DeleteItemByTestId(testId);
            _testRepository.Delete(testId);
            ViewBag.Result = _testRepository.GetAll();
            return Redirect("Index");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Entities;
using Test.Repositories;

namespace Test.WebUI.Controllers
{
    public class TestItemController : Controller
    {
        private ITestItemRepository _testItemRepository;
        private IAnswerRepository _answerRepository;
        private readonly ITestRepository _test;

        public TestItemController(ITestItemRepository testItemRepository, IAnswerRepository answerRepository, ITestRepository test)
        {
            _testItemRepository = testItemRepository;
            _answerRepository = answerRepository;
            _test = test;
        }

        [HttpPost]
        public ActionResult AddQuestion(string question, int idTest)
        {
            ViewBag.Error = "must be not empty or less 128 chars";
            if (question.Length <= 128 && !string.IsNullOrWhiteSpace(question))
            {
                var testItem = new Test.Entities.TestItemEntity
                {
                    Question = question,
                    TestId = idTest
                };
                ViewBag.Error = null;
                _testItemRepository.Add(testItem);
            }
            var thisItem = _test.GetElementById(idTest);
            Models.Test model = new Models.Test
            {
                Id = thisItem.Id,
                Name = thisItem.Name,
                Description = thisItem.Description,
                TestItems = _testItemRepository.GetItemsWhereTestId(thisItem.Id)
            };
            ViewBag.Result = model;
            return Redirect(String.Format("../Test/Edit?id={0}", idTest));
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var testId = _testItemRepository.GetItemById(id).TestId;
            _testItemRepository.Delete(id);
            var thisItem = _test.GetElementById(testId);
            Models.Test model = new Models.Test
            {
                Id = thisItem.Id,
                Name = thisItem.Name,
                Description = thisItem.Description,
                TestItems = _testItemRepository.GetItemsWhereTestId(thisItem.Id)
            };
            ViewBag.Result = model;
            return Redirect(String.Format("../Test/Edit?id={0}", testId));
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Entities;

namespace Test.WebUI.Models
{
    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<TestItemEntity> TestItems  { get; set; }
    }
}
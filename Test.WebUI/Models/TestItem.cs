﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Entities;

namespace Test.WebUI.Models
{
    public class TestItem
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public List<AnswerEntity> Answers { get; set; }
    }
}